<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class PertanyaanController extends Controller
{
    public function create(){
        return view('pertanyaan.create_pertanyaan');
    }

    public function store(Request $request){
       
        $request->validate([
            'judul' => 'required',
            'isi' => 'required'
        ]);

        $query = DB::table('pertanyaan')->insert([
            "judul" => $request["judul"],
            "isi" => $request["isi"]
        ]);

        return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil Disimpan!');
    }

    public function index(){
        $pertanyaan = DB::table('pertanyaan')->get();
       
        return view('pertanyaan.list_pertanyaan', compact('pertanyaan'));
    }

    public function show($id){
        $pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();
        
        return view('pertanyaan.detail_pertanyaan', compact('pertanyaan'));
    }
    public function edit($id){
        $pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();

        return view('pertanyaan.edit_pertanyaan', compact('pertanyaan'));
    }

    public function update($id, Request $request){

        $request->validate([
            'judul' => 'required',
            'isi' => 'required'
        ]);

        $query = DB::table('pertanyaan')
        ->where('id', $id)
        ->update([
            "judul" => $request["judul"],
            "isi" => $request["isi"]
        ]);

        return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil Diupdate!');
    }
      
    public function destroy($id){
        $query = DB::table('pertanyaan')->where('id', $id)->delete();
        return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil Dihapus!');
    }
}
