<div class="card">
              <div class="card-header">
                <h3 class="card-title">Pertanyaan Table</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              @if(session('success'))
                <div class="alert alert-success">
                  {{ session('success')}}
                </div>
              @endif
              <a class="btn btn-primary mb-2" href="/pertanyaan/create"> + Create New Pos</a>
              <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Judul</th>
                      <th>Isi</th>
                      <th style="width: 40px">Label</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($pertanyaan as $key => $tanya)
                      <tr>
                      <td> {{$key + 1}} </td>
                      <td> {{ $tanya->judul}} </td>
                      <td> {{$tanya->isi}} </td>
                      <td style="display: flex;">
                      <a href="pertanyaan/{{$tanya->id}}" class="btn btn-info btn-sm">show</a>
                      <a href="pertanyaan/{{$tanya->id}}/edit" class="btn btn-default btn-sm">edit</a>
                      <form action="pertanyaan/{{$tanya->id}}" method="DELETE">                    
                      <input type="submit" value="delete" class="btn btn-danger btn-sm">
                      </form>
                      </td>
                      </tr>
                      
                    @endforelse
                  </tbody>
                </table>
              </div>
             
            </div>

              

